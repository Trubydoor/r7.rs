(define (not x)
  (if x #f #t))

(define (null? obj)
  (if (eqv? obj '()) #t #f))

(define (id obj) obj)

(define (curry func arg1)  (lambda (arg) (apply func (cons arg1 (list arg)))))
