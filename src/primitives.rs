use value::*;
use std::ops::*;
use std::rc::Rc;
use std::collections::LinkedList;
use num::{Integer, zero, one};
use itertools::Itertools;
use eval::Evaluator;

pub fn add(args: LinkedList<Value>) -> Result<Value, String> {
    let n = args.iter().flat_map(Value::get_num).fold(zero(), Add::add);
    Ok(Value::Number(n))
}

pub fn mul(args: LinkedList<Value>) -> Result<Value, String> {
    let n = args.iter().flat_map(Value::get_num).fold(one(), Mul::mul);
    Ok(Value::Number(n))
}

pub fn sub(args: LinkedList<Value>) -> Result<Value, String> {
    let n = try!(args.iter().flat_map(Value::get_num).fold1(Sub::sub).ok_or("not a valid number"));
    Ok(Value::Number(n))

}

fn and(x:&bool, y:&bool) -> bool {
    *x && *y
}

fn or(x:&bool, y:&bool) -> bool {
    *x || *y
}

fn car(value : &Value) -> Result<Value, String> {
    match *value {
        Value::List(ref x) | Value::DottedList(ref x, _) => {
            let front = try!(x.front().ok_or("Called car on empty list"));
            Ok(front.clone())
        }
        ref v => Err(format!("Type mismatch, expected pair, recieved {}", v))
    }
}

fn cdr(value : &Value) -> Result<Value, String> {
    match *value {
        Value::List(ref x) => Ok(Value::List(x.iter().cloned().skip(1).collect())),
        Value::DottedList(ref xs, ref y) if xs.len() == 1 => Ok(*y.clone()),
        Value::DottedList(ref xs, ref y) => Ok(Value::DottedList(
                                                xs.iter().cloned().skip(1).collect(),
                                                y.clone())),
        ref v => Err(format!("Type mismatch, expected pair, recieved {}", v))
    }
}

fn cons(args : LinkedList<Value>) -> Result<Value, String> {
    let lhs = try!(args.front().ok_or(
        format!("Wrong number of arguments to cons. Expected 2 got {:?}.", args)));
    let rhs = try!(args.iter().nth(1).ok_or(
        format!("Wrong number of arguments to cons. Expected 2 got {:?}.", args)));
    let res = match *rhs {
        Value::List(ref l) if l.is_empty() => Value::List(collect![lhs.clone()]),
        Value::List(ref l) => {
            let mut new = l.clone();
            new.push_front(lhs.clone());
            Value::List(new)
        },
        Value::DottedList(ref l, ref last) => {
            let mut new = l.clone();
            new.push_front(lhs.clone());
            Value::DottedList(new, last.clone())
        },
        ref v => Value::DottedList(collect![lhs.clone()], Box::new(v.clone()))
    };

    Ok(res)
}

fn apply(eval : &mut Evaluator, args : &LinkedList<Value>) -> Result<Value, String> {
    let fun = try!(args.front().ok_or(
        format!("Wrong number of arguments to apply. Expected 2 got {:?}", args)));
    let args = try!(args.iter().nth(1).ok_or(
        format!("Wrong number of arguments to apply. Expected 2 got {:?}", args)));

    if let Value::List(arg_list) = args.clone() {
        match fun.clone() {
            Value::Primitive(f) => f.run(arg_list),
            _ => {
                let mut arg_list = arg_list.clone();
                arg_list.push_front(fun.clone());
                eval.eval_fun(fun, &arg_list)
            }
        }
    } else {
        Err(format!("Wrong type argument to apply. Expected list got {:?}", args))
    }
}

fn eval(eval : &mut Evaluator, args : &LinkedList<Value>) -> Result<Value, String> {
    if args.len() > 1 {
        Err(format!("Too many arguments to eval, expected 1 got {:?}", args))
    } else {
        let form = try!(args.front().ok_or(format!("No arguments given to eval")));
        eval.eval(form)
    }
}

pub fn conv_primitives(value: &Value) -> Result<Value, String> {
    match *value {
        Value::Atom(ref s) => match s.as_ref() {
            "+" => Ok(Value::Primitive(Primitive{name: "+", body: Rc::new(add)})),
            "-" => Ok(Value::Primitive(Primitive{name: "-", body: Rc::new(sub)})),
            "*" => Ok(Value::Primitive(Primitive{name: "*", body: Rc::new(mul)})),
            "mod" => numeric_prim!("mod", mod_floor),
            "quotient" => numeric_prim!("quotient", div_floor),
            "remainder" => numeric_prim!("remainder", rem),
            "number?" => type_prim!("number?", Number),
            "string?" => type_prim!("string?", String),
            "boolean?" => type_prim!("boolean?", Bool),
            "list?" => type_prim!("list?", List),
            "symbol?" => type_prim!("symbol?", Atom),
            "symbol->string" => convert_prim!("symbol->string", Atom -> String),
            "string->symbol" => convert_prim!("string->symbol", String -> Atom),
            "=" => bool_prim!("=", PartialEq::eq, Number),
            "<" => bool_prim!("<", PartialOrd::lt, Number),
            ">" => bool_prim!(">", PartialOrd::gt, Number),
            "/=" => bool_prim!("/=", PartialOrd::ne, Number),
            "<=" => bool_prim!("<=", PartialOrd::le, Number),
            ">=" => bool_prim!(">=", PartialOrd::ge, Number),
            "&&" | "and" => bool_prim!("and", and, Bool),
            "||" | "or" => bool_prim!("or", or, Bool),
            "string=?" => bool_prim!("string=?", PartialEq::eq, String),
            "string<?" => bool_prim!("string<?", PartialOrd::lt, String),
            "string>?" => bool_prim!("string>?", PartialOrd::gt, String),
            "string<=?" => bool_prim!("string<=?", PartialOrd::le, String),
            "string>=?" => bool_prim!("string>=?", PartialOrd::ge, String),
            "car" => list_prim!("car", car),
            "cdr" => list_prim!("cdr", cdr),
            "cons" => Ok(Value::Primitive(Primitive{name: "cons", body: Rc::new(cons)})),
            "eqv?" => bool_prim!("eqv?", PartialEq::eq),
            "eq?" => bool_prim!("eq?", PartialEq::eq),
            "apply" => Ok(Value::Eval(Eval{name: "apply", body: Rc::new(apply)})),
            "eval" => Ok(Value::Eval(Eval{name: "eval", body: Rc::new(eval)})),
            _ => Ok(value.clone())
        },
        _ => Ok(value.clone())
    }
}
