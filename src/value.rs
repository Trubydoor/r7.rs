use std::collections::LinkedList;
use num::bigint::BigInt;
use std::fmt;
use std::rc::Rc;
use eval::Evaluator;

#[derive(PartialEq, Clone)]
pub struct Function {
    pub args : Vec<String>,
    pub body : Rc<Value>,
}

impl fmt::Display for Function{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(lambda ({}) {})", self.args.join(" "), *self.body)
    }
}

#[derive(Clone)]
pub struct Primitive {
    pub name : &'static str,
    pub body : Rc<Fn(LinkedList<Value>) -> Result<Value, String>>
}

impl Primitive {
    pub fn run(&self, args: LinkedList<Value>) -> Result<Value, String> {
        let f = &self.body;
        f(args)
    }
}

impl PartialEq for Primitive {
    fn eq(&self, other : &Self) -> bool {
        self.name == other.name
    }
}

#[derive(Clone)]
pub struct Eval {
    pub name : &'static str,
    pub body : Rc<Fn(&mut Evaluator, &LinkedList<Value>) -> Result<Value, String>>
}

impl Eval {
    pub fn run(&self, eval : &mut Evaluator, args : &LinkedList<Value>) -> Result<Value, String> {
        let f = &self.body;
        f(eval, args)
    }
}

impl PartialEq for Eval {
    fn eq(&self, other : &Self) -> bool {
        self.name == other.name
    }
}

#[derive(PartialEq, Clone)]
pub enum Value {
    Atom(String),
    List (LinkedList<Value>),
    DottedList (LinkedList<Value>, Box<Value>),
    Number(BigInt),
    String(String),
    Bool(bool),
    Character(char),
    Function(Function),
    Primitive(Primitive),
    Eval(Eval)
}

impl Value {
    pub fn get_num(&self) -> Result<BigInt, String> {
        match *self {
            Value::Number(ref n) => Ok(n.clone()),
            _ => Err("Not a number".to_string())
        }
    }
}

impl Default for Value {
    fn default() -> Self {
        Value::List(LinkedList::new())
    }
}

fn unwords(lst : LinkedList<Value>) -> String {
    let str = lst.iter().fold("".to_string(), |acc, x| format!("{} {}", acc, x));
    if str.len() == 0 {
        "".to_string()
    } else {
        (&str[1..str.len()]).to_string()
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.clone() {
            Value::String(c) => write!(f, "\"{}\"", c),
            Value::Atom(c) => write!(f, "{}", c),
            Value::Number(n) => write!(f, "{}", n),
            Value::Bool(b) => write!(f, "{}", if b {"#t"} else {"#f"}),
            Value::List(l) => write!(f, "({})", unwords(l)),
            Value::DottedList(l, v) => write!(f, "({} . {})", unwords(l), v),
            Value::Character(c) => write!(f, "'{}'", c),
            Value::Function(fun) => write!(f, "{}", fun),
            Value::Primitive(p) => write!(f, "{}", p.name),
            Value::Eval(p) => write!(f, "{}", p.name)
        }
    }
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl ::std::iter::FromIterator<Value> for Value {
    fn from_iter<T>(iter: T) -> Self where T: IntoIterator<Item=Value> {
        Value::List(iter.into_iter().collect())
    }
}
