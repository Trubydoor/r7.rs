use value::Value;
use std::collections::HashMap;

pub type Env = HashMap<String, Value>;

pub fn is_bound(env: &Env, var: &str) -> bool {
    env.contains_key(var)
}

pub fn get_var(env: &Env, var: &str) -> Result<Value, String> {
    if !is_bound(env, var) {
        Err(format!("Attempt to fetch unbound variable {}", var))
    } else {
        Ok(env[var].clone())
    }
}

pub fn set_var(env: &mut Env, var: &str, val: &Value) -> Result<Value, String> {
    if !is_bound(env, var) {
        Err("Attempt to set unbound variable".to_string())
    } else {
        env.insert(var.to_string(), val.clone());
        Ok(val.clone())
    }
}

pub fn define_var(env: &mut Env, var: &str, val: &Value) -> Value {
    env.insert(var.to_string(), val.clone());
    val.clone()
}

pub fn bind_vars(env: &Env, bindings: &HashMap<String, Value>) -> Env {
    let mut env = env.clone();
    for (var, val) in bindings {
        env.insert(var.clone(),val.clone());
    }
    env
}
