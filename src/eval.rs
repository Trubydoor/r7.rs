use value::{Value, Primitive, Function};
use std::collections::LinkedList;
use variables::*;
use std::rc::Rc;
use primitives::conv_primitives;

#[derive(Default)]
pub struct Evaluator {
    env: Env
}

impl Evaluator {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn new_env(env: &Env) -> Self {
        Evaluator {env: env.clone()}
    }

    pub fn eval(&mut self, val: &Value) -> Result<Value, String> {
        match *val {
            //Value::String(_) | Value::Number(_) | Value::Bool(_) => Ok(val.clone()),
            Value::List(ref l) => self.eval_list(l),
            Value::Atom(ref id) => get_var(&self.env, id),
            //Value::Primitive(_) => Ok(val.clone()),
            //Value::Eval(_) => Ok(val.clone()),
            _ => Ok(val.clone())
        }
    }

    fn if_(&mut self, list: &LinkedList<Value>) -> Result<Value, String> {
        let condition = try!(list.iter().nth(1).ok_or("Missing if condition"));
        let branch = if let Value::Bool(false) = try!(self.eval(condition)) {
            // Return empty list if no else branch given.
            list.iter().nth(3).cloned().unwrap_or_default()
        } else {
            // Error if no then branch given.
            try!(list.iter().nth(2).cloned().ok_or("Missing then branch"))
        };
        self.eval(&branch)
    }

    fn define(&mut self, list: &LinkedList<Value>) -> Result<Value, String> {
        let declaration = try!(list.iter().nth(1).ok_or(
                    format!("No declaration given to define in expression {:?}", list)));
        match *declaration {
            Value::Atom(ref name) => {
                // Default to empty list if no definition is given.
                let definition = list.iter().nth(2).cloned().unwrap_or_default();
                let evaluated_definition = try!(self.eval(&definition));
                Ok(define_var(&mut self.env, &name, &evaluated_definition))
            }
            Value::List(ref l) => {
                let mut decl : LinkedList<String> = l.iter().filter_map(|val|
                    if let Value::Atom(ref s) = *val {
                        Some(s.clone())
                    } else {None}
                ).collect();

                let name = try!(decl.pop_front().ok_or("function name mising"));
                let body = try!(list.iter().nth(2).cloned().ok_or("function body missing"));
                let val = Value::Function(Function{args:decl.iter().cloned().collect(),
                                                   body:Rc::new(body)});
                Ok(define_var(&mut self.env, &name, &val))
            }
            _ => Err("Not a valid definition".to_string())
        }
    }

    fn lambda(&mut self, list: &LinkedList<Value>) -> Result<Value, String> {
        let args = try!(list.iter().nth(1).ok_or("No arguments given for lambda"));
        if let Value::List(ref l) = *args {
            let decl = l.iter().map(|val| match *val {
                Value::Atom(ref s) => Ok(s.clone()),
                _ => Err("Not a valid lambda")
            });

            let body = try!(list.iter().nth(2).ok_or("No body given for lambda"));

            Ok(Value::Function(Function{args:try!(decl.collect()),
                               body:Rc::new(body.clone())}))
        } else {
            Err("not a valid lambda".to_string())
        }
    }

    fn set(&mut self, list: &LinkedList<Value>) -> Result<Value, String> {
        let first = try!(list.iter().nth(1).ok_or("No name given to set"));
        let name = match *first {
            Value::Atom(ref name) => Ok(name.clone()),
            _ => Err("Not a string literal name".to_string())
        };
        let val = try!(list.iter().nth(2).ok_or("No valid value for variable given"));
        let val = try!(self.eval(val));
        set_var(&mut self.env, &try!(name), &val)
    }

    pub fn eval_fun(&mut self, fun: &Value, list: &LinkedList<Value>) -> Result<Value, String> {
        let fun = try!(conv_primitives(&fun));
        match try!(self.eval(&fun)) {
            Value::Function(ref f) => {
                let args : Vec<Value> =
                    try!(list.iter().skip(1).cloned().map(|v| self.eval(&v)).collect());

                if f.args.len() != args.len() {
                    return Err(format!("wrong number of arguments to function. Expected {} got {:?}", f.args.len(), args))
                };
                let env = bind_vars(&self.env, &f.args.iter().cloned().zip(args).collect());
                let mut eval = Evaluator::new_env(&env);
                eval.eval(&f.body.clone())
            },
            Value::Primitive(ref f) => {
                let args = try!(list.iter().skip(1).cloned().map(|v| self.eval(&v)).collect());
                self.run_primitive(f, args)
            },
            Value::Eval(ref f) => {
                let form = try!(list.iter().skip(1).map(|v| self.eval(v)).collect());
                f.run(self, &form)
            }
            ref f => Err(format!("{} is not a function", f))
        }
    }

    fn eval_list(&mut self, list : &LinkedList<Value>) -> Result<Value, String> {
        let first = try!(list.front().ok_or("Attempted to evaluate empty list"));
        match *first {
            Value::Atom(ref a) if a == "quote" => {
                let arg = try!(list.iter().nth(1).ok_or("Missing argument to quote"));
                Ok(arg.clone())
            },
            Value::Atom(ref a) if a == "if" => {
                self.if_(&list)
            },
            Value::Atom(ref a) if a == "define" => {
                self.define(&list)
            },
            Value::Atom(ref a) if a == "lambda" => {
                self.lambda(&list)
            }
            Value::Atom(ref a) if a == "set!" => {
                self.set(&list)
            },
            /*Value::Atom(ref a) if a == "eval" => {
                let form = try!(list.into_iter().nth(1).ok_or(format!("Not enough arguments to eval. Expected 2 got {:?}", list)));
                let form = try!(self.eval(form));
                self.eval(&form)
            },*/
            Value::Primitive(ref s) =>  {
                let args = list.iter().skip(1).cloned().collect();
                self.run_primitive(&s, args)
            },
            Value::Eval(ref f) => {
                let form = try!(list.iter().skip(1).map(|v| self.eval(v)).collect());
                f.run(self, &form)
            }
            ref fun@Value::Atom(_) => {
                self.eval_fun(&fun, &list)
            },
            _ => Err(format!("{} is not a function", first))
        }
    }

    fn run_primitive(&mut self, primitive : &Primitive, args : LinkedList<Value>) -> Result<Value, String> {
        let args = try!(args.iter().map(|v| self.eval(v)).collect());
        primitive.run(args)
    }
}
