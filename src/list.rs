use gc::Gc;
use value::Value;

#[derive(Clone)]
pub enum List {
    Cell(Value,Gc<List>),
    Nil
}

impl List {
    pub fn new() -> Self {
        List::Nil
    }

    pub fn car(self) -> Option<Value> {
        if let List::Cell(v, _) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn cdr(self) -> Option<Gc<List>> {
        if let List::Cell(_, list) = self {
            Some(list)
        } else {
            None
        }
    }

    pub fn cons(self, v: &Value) -> Self {
        List::Cell(v.clone(), Gc::new(self))
    }

    pub fn into_iter(self) -> IntoIter {
        IntoIter(self)
    }
}

impl Default for List {
    fn default() -> Self {
        List::new()
    }
}

pub struct IntoIter(List);

impl Iterator for IntoIter {
    type Item = Value;
    fn next(&mut self) -> Option<Self::Item> {
        let v = self.0.clone().car();
        self.0 = (*self.0.clone().cdr().unwrap_or_default()).clone();
        v
    }
}
