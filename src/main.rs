extern crate regex;
extern crate num;
extern crate linenoise;
#[macro_use] extern crate itertools;
pub mod lexer;
pub mod parser;
#[macro_use] pub mod primitive_macros;
pub mod value;
pub mod variables;
pub mod eval;
pub mod primitives;
pub mod gc;
pub mod list;
use std::fs;
use std::io::Read;

fn main() {
    let mut eval = eval::Evaluator::new();

    for lib in fs::read_dir("std").unwrap().filter_map(Result::ok) {
        let mut libstr = String::new();
        let mut lib = fs::File::open(lib.path()).unwrap();
        let _ = lib.read_to_string(&mut libstr);
        match lexer::tokenize(&libstr) {
            Ok(tokens) => for expr in parser::parse(&tokens) {
                for expr in expr {
                    match eval.eval(&expr) {
                        Err(err) => println!("Error: {}", err),
                        _ => continue
                    }
                }
            },
            Err(err) => println!("Failed to tokenise: {}", err)
        }
    }

    linenoise::set_multiline(0);
    loop {
        match linenoise::input("-> ") {
            None => break,
            Some(mut input) => {
                linenoise::history_add(&input);
                while !balanced(&input) {
                    match linenoise::input(">> ") {
                        None => break,
                        Some(moreinput) => input = input + &moreinput
                    }
                }
                match lexer::tokenize(&input) {
                    Ok(tokens) => for expr in parser::parse(&tokens) {
                                    for expr in expr {
                                      match eval.eval(&expr) {
                                          Ok(res) => println!("{}", res),
                                          Err(err) => println!("Error: {}", err)
                                      }
                                    }
                    },
                    Err(err) => println!("Failed to tokenise: {}", err)
                }
            }
        }
    }

}

fn balanced(string: &str) -> bool {
    if string.is_empty() {
        return true;
    }

    let mut stack = Vec::new();
    for c in string.chars() {
        if c == '(' {
            stack.push(c);
        } else if c == ')' {
            if stack.is_empty() {
                return false;
            }

            stack.pop();
        }
    }

    stack.is_empty()
}
