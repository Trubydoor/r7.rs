macro_rules! is_type {
    ( $name:expr, $x:ident, $args:expr ) => {{
        let args = try!($args.iter().nth(0).ok_or(
            format!("Wrong number of arguments to {}. Expected {} got {:?}", $name, 1, $args)));
        let b = if let Value::$x(_) = *args {
            true
        } else {
            false
        };
        Ok(Value::Bool(b))
    }};
}

macro_rules! type_prim {
    ($name:expr, $t:ident) => {{
        let f = ::std::rc::Rc::new(|args : LinkedList<Value>| {is_type!($name, $t, args)});
        Ok(Value::Primitive(Primitive{name:$name, body: f}))
    }}
}

macro_rules! convert_type {
    ( $name:expr, $x:ident -> $y:ident , $args:expr) => {{
        let args = try!($args.front().ok_or(
            format!("Wrong number of arguments to {}. Expected 1 got {:?}", $name, $args)));
        let v = match *args {
            Value::$x(ref a) => Ok(a.clone()),
            _ => Err("Failed to convert type")
        };
        Ok(Value::$y(try!(v)))
    }};
}

macro_rules! convert_prim {
    ($name:expr, $x:ident -> $y:ident) => {{
        let f = ::std::rc::Rc::new(|args: LinkedList<Value>| {convert_type!($name, $x -> $y, args)});
        Ok(Value::Primitive(Primitive{name: $name, body: f}))
    }}
}

macro_rules! numeric_fn {
    ( $args : ident, $x : ident ) => {{
            assert!($args.len() == 2);
            let lhs = try!(try!($args.front().ok_or("Numeric function error")).get_num());
            let rhs = try!(try!($args.back().ok_or("Numeric function error")).get_num());
            Ok(Value::Number(lhs.$x(&rhs)))
    }};
}

macro_rules! numeric_prim {
    ($name:expr, $x:ident) => {{
        let f = ::std::rc::Rc::new(|args : LinkedList<Value>| {numeric_fn!(args, $x)});
        Ok(Value::Primitive(Primitive{name: $name, body: f}))
    }}
}

macro_rules! bool_fn {
    ($name:expr, $x:path, $args:expr) => {{
        let lhs = try!($args.iter().nth(0).ok_or(
            format!("Too few arguments to {}. Expected 2 got {:?}.", $name, $args)));
        let rhs = try!($args.iter().nth(1).ok_or(
            format!("Too few arguments to {}. Expected 2 got {:?}.", $name, $args)));
        Ok(Value::Bool($x(lhs,rhs)))
    }};
    ($name:expr, $x:path, $args:expr, $z:ident) => {{
        let lhs = try!($args.iter().nth(0).ok_or(
            format!("Too few arguments to {}. Expected 2 got {:?}.", $name, $args)));
        let rhs = try!($args.iter().nth(1).ok_or(
            format!("Too few arguments to {}. Expected 2 got {:?}", $name, $args)));
        let args = match (lhs,rhs) {
            (&Value::$z(ref r), &Value::$z(ref l)) => Ok((r,l)),
            _ => Err("Wrong type of arguments to boolean expression")
        };
        let (lhs,rhs) = try!(args);
        let b = $x(&lhs,&rhs);
        Ok(Value::Bool(b))
    }}
}

macro_rules! bool_prim {
    ($name:expr, $fun:path) => {{
        let f = ::std::rc::Rc::new(|args: LinkedList<Value>| {bool_fn!($name, $fun, args)});
        Ok(Value::Primitive(Primitive{name: $name, body: f}))
    }};
    ($name:expr, $fun:path, $t:ident) => {{
        let f = ::std::rc::Rc::new(|args : LinkedList<Value>| {bool_fn!($name, $fun, args, $t)});
        Ok(Value::Primitive(Primitive{name: $name, body: f}))
    }}
}

macro_rules! list_fn {
    ($x:ident, $y:expr) => {{
        let arg = try!($y.front().ok_or(format!("Wrong number of arguments to list function. Expected 1 got {}.", $y.len())));
        $x(arg)
    }}
}

macro_rules! list_prim {
    ($name:expr, $f:ident) => {{
        let f = ::std::rc::Rc::new(|args : LinkedList<Value>| {list_fn!($f, args)});
        Ok(Value::Primitive(Primitive{name: $name, body: f}))
    }}
}

macro_rules! count_exprs {
    () => { 0 };
    ($e:expr) => { 1 };
    ($e:expr, $($es:expr),+) => { 1 + count_exprs!($($es),*) };
}

macro_rules! collect {
    // Short-hands for initialising an empty collection.
    [] => { collect![into _] };
    [into $col_ty:ty] => { collect![into $col_ty:] };
    [into $col_ty:ty:] => {
        {
            let col: $col_ty = ::std::default::Default::default();
            col
        }
    };

    // Initialise a sequence with a constrained container type.
    [into $col_ty:ty: $v0:expr] => { collect![into $col_ty: $v0,] };

    [into $col_ty:ty: $v0:expr, $($vs:expr),* $(,)*] => {
        {
            use std::marker::PhantomData;

            const NUM_ELEMS: usize = count_exprs!($v0 $(, $vs)*);

            // This trick is stolen from std::iter, and *should* serve to give the container enough information to pre-allocate sufficient storage for all the elements.
            struct SizeHint<E>(PhantomData<E>);

            impl<E> SizeHint<E> {
                // This method is needed to help the compiler work out which `Extend` impl to use in cases where there is more than one (e.g. `String`).
                #[inline(always)]
                fn type_hint(_: &E) -> SizeHint<E> { SizeHint(PhantomData) }
            }

            impl<E> Iterator for SizeHint<E> {
                type Item = E;

                #[inline(always)]
                fn next(&mut self) -> Option<E> {
                    None
                }

                #[inline(always)]
                fn size_hint(&self) -> (usize, Option<usize>) {
                    (NUM_ELEMS, Some(NUM_ELEMS))
                }
            }

            let mut col: $col_ty = ::std::default::Default::default();
            let v0 = $v0;

            Extend::extend(&mut col, SizeHint::type_hint(&v0));

            Extend::extend(&mut col, Some(v0).into_iter());
            $(Extend::extend(&mut col, Some($vs).into_iter());)*

            col
        }
    };

    // Initialise a sequence with a fully inferred contained type.
    [$($vs:expr),+] => { collect![into _: $($vs),+] };

    // Initialise a map with a constrained container type.
    [into $col_ty:ty: $($ks:expr => $vs:expr),+] => {
        // Maps implement FromIterator by taking tuples, so we just need to rewrite each `a:b` as `(a,b)`.
        collect![into $col_ty: $(($ks, $vs)),+]
    };

    // Initialise a map with a fully inferred contained type.
    [$($ks:expr => $vs:expr),+] => { collect![into _: $($ks => $vs),+] };
}
