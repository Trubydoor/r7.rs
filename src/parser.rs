use lexer::Token;
use std::collections::LinkedList;
use value::Value;
use num::BigInt;
use num::Num;
use primitives::conv_primitives;

fn atom(atom:String) -> Value {
    match atom.as_ref() {
            "#t" => Value::Bool(true),
            "#f" => Value::Bool(false),
            _ => Value::Atom(atom)
    }
}

fn number(c:String) -> Result<Value, String> {
    let n = if c.starts_with("#b") {
        BigInt::from_str_radix(&c[2..], 2)
    } else if c.starts_with("#o") {
        BigInt::from_str_radix(&c[2..], 8)
    } else if c.starts_with("#x") {
        BigInt::from_str_radix(&c[2..], 16)
    } else if c.starts_with("#d") {
        BigInt::from_str_radix(&c[2..], 10)
    } else {
        BigInt::from_str_radix(&c, 10)
    };

    let num = match n {
        Ok(ok) => ok,
        Err(err) => return Err(format!("{}", err))
    };

    Ok(Value::Number(num))
}

fn string(c:String) -> Value {
    let mut res = c.replace(r"\a", "\u{0007}");
    res = res.replace(r"\b", "\u{0009}");
    res = res.replace(r"\t", "\t");
    res = res.replace(r"\n","\n");
    res = res.replace(r"\r", "\r");
    res = res.replace(r"\\", r"\");
    res = res.replace(r"\|", "\u{007C}");
    Value::String(res)
}

fn character(chars:String) -> Result<Value, String> {
    match chars.to_lowercase().as_ref() {
        r"#\space" => Ok(Value::Character(' ')),
        r"#\newline" => Ok(Value::Character('\n')),
        _ => {
            let char = try!(chars.chars().nth(2).ok_or("not a valid UTF-8 character"));
            Ok(Value::Character(char))
        }
    }
}

fn list(lst : &[Token]) -> Result<(Value, usize), String> {
    let mut x = 1;
    let mut idx = 0;
    for tok in lst {
        idx += 1;
        if *tok == Token::OpenParen {
            x += 1;
        } else if *tok == Token::CloseParen {
            x -= 1;
        }
        if x == 0 {
            break;
        }
    }
    idx = idx - 1;

    let parsed = try!(parse(&lst[0..idx]));
    let dotted = if parsed.len() > 1 && parsed[parsed.len()-2] == Value::Atom(".".to_string()) {
        Value::DottedList(parsed[0..parsed.len()-2].iter().cloned().collect(), Box::new(parsed[parsed.len()-1].clone()))
    } else {
        Value::List(parsed.iter().cloned().collect())
    };

    Ok((dotted, idx))
}

pub fn quote(vec : &[Value]) -> Vec<Value> {
    let mut quoted = Vec::new();
    let mut i = 0;
    while i != vec.len() {
        let sym = if vec[i] == Value::Atom("\'".to_string()) {
            "quote"
        } else if vec[i] == Value::Atom("`".to_string()) {
            "quasiquote"
        } else if vec[i] == Value::Atom(",".to_string()) {
            "unquote"
        } else {""};
        if sym != "" {
            let mut lst = LinkedList::new();
            lst.push_back(Value::Atom(sym.to_string()));
            lst.push_back(vec[i+1].clone());
            quoted.push(Value::List(lst));
            i += 1;
        } else {
            quoted.push(vec[i].clone());
        }
        i += 1;
    }
    quoted
}

pub fn parse(tokens: &[Token]) -> Result<Vec<Value>, String> {
    let mut vec = Vec::new();
    let mut i = 0;
    while i != tokens.len() {
        let v = match tokens[i].clone() {
            Token::Atom(c) => atom(c),
            Token::Str(c) => string(c),
            Token::Number(c) => try!(number(c)),
            Token::Char(c) => try!(character(c)),
            Token::OpenParen => {
                let (val, skip) = try!(list(&tokens[i+1..]));
                i += skip+1;
                val
            },
            _ => return Err("Can not parse".to_string())
        };
        vec.push(v);
        i += 1;
    }
    vec = quote(&vec);
    vec = try!(vec.iter().map(conv_primitives).collect());
    Ok(vec)
}
