use regex;
use regex::{Regex};
use self::Token::*;

#[derive(PartialEq, Clone, Debug)]
pub enum Token {
    Str(String),
    Symbol(String),
    Atom(String),
    Number(String),
    Char(String),
    Function(String),
    Quote,
    OpenParen,
    CloseParen,
}

pub fn tokenize(input: &str) -> Result<Vec<Token>, String> {
    let comments = Regex::new(r"(?m);.*\n");
    let comments = match comments {
        Ok(t) => t,
        Err(err) => return Err(format!("{}", err))
    };

    // Remove comments from input.
    let preprocessed = comments.replace_all(input, "\n");

    let tokens = Regex::new(concat!(
        r"(?P<quote>[',`])|",
        r"(?P<oparen>\()|",
        r"(?P<cparen>\))|",
        r"(?P<number>(((#d)|(#b)|(#o))?\d+)|(#x([a-fA-F]|\d)+))|",
        r"(?P<char>#\\(space|newline|.))|",
        r"(?P<atom>((\p{Alphabetic})|[-!#$%&|*+/:<=>?@^_~])[^\(^\s)]*|\.)|",
        r"(?P<symbol>[-!#$%&|*+/:<=>?@^_~])|",
        r#""(?P<string>([^"]|(\\"))*)""#,
    ));

    let tokens = match tokens {
        Ok(t) => t,
        Err(err) => return Err(format!("{}", err))
    };

    tokens.captures_iter(&preprocessed).map(match_token).collect()
}

fn match_token(token: regex::Captures) -> Result<Token, String> {
    if token.name("oparen").is_some() {
        Ok(OpenParen)
    } else if token.name("cparen").is_some() {
        Ok(CloseParen)
    } else if let Some(symbol) = token.name("symbol") {
         Ok(Symbol(symbol.to_string()))
    } else if let Some(atom) = token.name("atom") {
        Ok(Atom(atom.to_string()))
    } else if let Some(string) = token.name("string") {
        Ok(Str(string.to_string()))
    } else if let Some(number) = token.name("number") {
        Ok(Number(number.to_string()))
    } else if let Some(char) = token.name("char") {
        Ok(Char(char.to_string()))
    } else if let Some(quote) = token.name("quote") {
        Ok(Atom(quote.to_string()))
    } else {
        Err("Failed to lex".to_string())
    }
}
